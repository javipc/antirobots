
# Anti Robots

Anti Robots protege formularios web (Captcha)
muestra al usuario una serie de imágenes aleatorias y luego unas miniaturas
el usuario deberá compararlas y elegir entre las miniaturas todas las que coincidan 
con las imágenes aleatorias presentadas
si las imágenes coinciden se le da acceso al usuario

* Puedes controlar la cantidad de dibujos.
* Puedes controlar la cantidad de opciones que recibe el usuario.
* El desafío es con imágenes, no con letras (parece un juego).
* Puedes personalizarlo con CSS.
* No requiere javascript.
* No requiere complementos externos.
* No utiliza Apis de empresas rastreadoras.
* No utiliza código propietario.
* Es LIBRE.



## Captura

<img src="captura.jpg" alt="Captura de Pantalla" height="200">
<img src="captura2.jpg" alt="Captura de Pantalla" height="200">



## ¿Por qué Anti Robots?

Lamentablemente todos los sitios webs están infectados de apis de empresas rastreadoras,
desarrollar un programa como un filtro que distinga usuarios humanos de autómatas lleva su tiempo,
por lo tanto para economizar recurren a apis de terceros. 
Como consecuencia el usuario se ve obligado a realizar conexiones con sitios de terceros,
por ese motivo decidí desarrollar este pequeño programa.






## Instalación 

incluye el programa en tu archivo PHP.

require_once("antirobot.php");



## Modo de uso

verifica si el código esta aprobado.

$codigoAprobado = antirobot();



visualiza el desafío.

graficar();






## Personalizar la cantidad de elementos

antirobot (cantidadDeOpciones);

graficar (cantidadDeDibujos, cantidadDeOpciones);





# Importante

Es solo un proyecto, por lo tanto solo podrá usarse con fines experimentales y
no con fines de seguridad.





# Más aplicaciones

Al no tener publicidad este proyecto se mantiene únicamente con donaciones.
Siguiendo este enlace tendrás más información y también más aplicaciones.
[Más información](https://gitlab.com/javipc/mas) 



