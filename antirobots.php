<?php



	/*
		Anti-Robots 
		
		Programado por Javier Martínez
		
		
		este programa protege formularios web (Captcha)
		muestra al usuario una serie de imágenes aleatorias y luego unas miniaturas
		el usuario deberá compararlas y elegir entre las miniaturas todas las que coincidan 
		con las imágenes aleatorias presentadas
		si las imágenes coinciden se le da acceso al usuario
				
					
				

		# Instalación 

		// incluye el programa en tu archivo php
		require_once("antirobot.php");



		# modo de uso 

		// verifica si el código esta aprobado
		$codigoAprobado = antirobot();

		// visualiza el desafío
		graficar();



		# Ejemplo 

		$codigoAprobado = antirobot();

		if($codigoAprobado == 0)
			echo "Intenta resolver este codigo";

		if($codigoAprobado == -1)
			echo "¡NOOO!... ¡TENES CARA DE ROBOT!";

		if($codigoAprobado == 1)
			echo "¡facil! ¿verdad?";

		graficar();




		# Personalizar la cantidad de elementos

		antirobot (cantidadDeOpciones);

		graficar (cantidadDeDibujos, cantidadDeOpciones);




		
		
		-----------------------------------------------------------------------------------
		
		Creditos
		http://www.forosdelweb.com/f18/como-implementar-captcha-formulario-html-php-844451/
		https://stackoverflow.com/questions/4148499/how-to-style-a-checkbox-using-css
		https://tng.community/index.php?/forums/topic/13606-imagettftext-could-not-findopen-font-in-php-7126/
		
		Este programa utiliza fuentes de https://fontawesome.com
		
		IMPORTANTE
		este programa es SOLO UN PROYECTO y no deberá utilizarse con fines de seguridad
		ni el desarrollador ni colaboradores son ni serán responsables por el uso o mal uso 
		de este programa
		
		
		Este programa es gratuito 
		podes hacer una donación :)
		http://javim.000webhostapp.com/donacion
	
	*/



	
		/*
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
	*/
		
			
			
			
			
			
			
			
			
			
			
			
			
	session_start();

	require_once("fa.php");
	
	
	

	// no se que es esto, pero debe estar aquí para que funcione todo
	//putenv('GDFONTPATH=' . realpath('.'));


	
	
	
	
	
	
	
	
	function antirobot ($cantidad=10) {
		
		if (intentandoComprobar())
			if (aprobado($cantidad)) 
				return 1;
			else
				return -1;
			
		return 0;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	function claveSesion() {
	if (isset($_SESSION['clavesesion']))
		return $_SESSION['clavesesion'];
	else
		return "-1";
	}
	
	
	
	function post($dato, $falso = "") {
	if (isset($_POST[$dato])) 
		return filter_var($_POST[$dato], FILTER_SANITIZE_STRING);
	else
		return $falso;
	}		
	
	function check_post ($dato) {
	if (isset($_POST[$dato])) 
			return true; 
		else
			return false;
	}

	


	

	

	function aprobado ($cantidad = 10) {
	
		if (post('controlantirobot', "") == "comprobar") {
			$respuesta = 0;
			for ($x = 0; $x <= $cantidad-1; $x=$x+1) 
				if (check_post("f$x"))
					$respuesta = $respuesta + post("c$x", 0);
							
			if ($respuesta == clavesesion())
				return true;
			else
				return false;
		}		
	}
		
	
	
	function intentandoComprobar () {
		if (post('controlantirobot', "") == "comprobar") 
			return true;
		else
			return false;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	// crea una imagen con un tamaño especifico
	function crearImagen ($x = 120, $y=100) {
		// crea la imagen
		$im = imagecreatetruecolor($x, $y);

		// colores
		$blanco = imagecolorallocate($im, 255, 255, 255);
		
		// rellena con un rectángulo
		imagefilledrectangle($im, 0, 0, $x, $y, $blanco);

		return $im;
	}
	
	
	
	
	
	
	// obtiene un arreglo con una serie de imágenes 
	// PUEDEN REPETIRSE - SON ALEATORIAS
	
	function obtenerClaves ($cantidad = 1) {
		$respuesta = array();
		if ($cantidad <=0) 
			return $respuesta;
		

	
		// cantidad de letras (dibujos) que existen;
		$min = 1;
		$max = 483;
		
		for ($x = 0; $x <= $cantidad-1; $x = $x +1) {
			
			// genera un numero aleatorio
			$aleatorio = rand ( $min ,  $max );
			
			// obtiene una imagen 
			$dato = infofa($aleatorio);
			
			// obtiene información de esa imagen
			$codigo = $dato["codigo"];
			$horizontal = $dato["horizontal"];
			$nombre = $dato["nombre"];	
			
			// vuelva los valores para obtener una respuesta
			$respuesta[$x]['dibujo'] = $aleatorio;      // el numero de dibujo (fa.php)
			$respuesta[$x]['codigo'] = $codigo;         // el texto
			$respuesta[$x]['nombre'] = $nombre;         // un nombre que describe la imagen
			$respuesta[$x]['horizontal'] = $horizontal; // quizás el tamaño
			$respuesta[$x]['id'] = pow (10,$x);          // un valor que se usara como clave única de la imagen 
			
		}
		
		return $respuesta;
	}
	
	
	// inserta texto (dibujo) en una imagen
	function insertarTexto ($imagen, $texto, $x =10, $y=80, $angulo=0) {
		
			
		// tipo de letra
		//$fuente = "./fa-solid-900.ttf";				
		$fuente =  dirname(__FILE__) . "/fa-solid-900.ttf";
		
		$gris = imagecolorallocate($imagen, 128, 128, 128);
		$negro = imagecolorallocate($imagen, 0, 0, 0);
		
		$tam = 58;
		
		imagettftext($imagen, $tam, $angulo, $x, $y, $negro, $fuente, $texto);
	}
	
	
	
	
	
	// realiza todo el proceso para generar un código
	
	function generarCodigo ( $total = 3 ) {
			
		if ($total <=0)
			return;
		
		;
	
		
		$imagen = array();
		// crea la imagen principal
	
		$clave = array();
		$clave = obtenerClaves($total);
		
		
		
		// crea una imagen únicamente con el dibujo (mini)
		for ($x = 0; $x <= $total-1; $x = $x +1) {			
			$imagen[$x] = crearImagen ();
			$texto = $clave[$x]["codigo"];
			insertarTexto($imagen[$x], $texto);
			$clave[$x]["imagen"] = $imagen[$x];
		}
		
		
		//$clave[0]["imagen"] = $imagen[0];
		
		return $clave;
	}
		
	
	
	function imagenPrincipal ($tam, $clave) {
			// tamaño de la imagen
		$dimension = (100 * $tam) +20;
		
		$principal = array();
		// crea la imagen principal
		$principal["imagen"] = crearImagen ($dimension);

		
		
		
		$principal["id"] = 0;
		
		// inserta el texto en la imagen principal
		for ($x = 0; $x <= $tam-1; $x = $x +1) {
			$posicion = 10 + (100 * $x );
			$angulo = rand(0,40) - 20;
			$texto = $clave[$x]["codigo"];
			insertarTexto ($principal["imagen"], $texto, $posicion,80,$angulo) ;
			$principal["id"] = $principal["id"] + $clave[$x]["id"];
			
		}
		
		return $principal;
	}
	
	
	
	
	
	
	
	
	
	
	function graficar($tam =3, $cantidad=6) {
	
	
		
		
		$codigo = array();
		$principal = array();
		
		
		// elige componentes, imágenes miniaturas
		$codigo = generarCodigo( $cantidad);
		
		// mezcla las imágenes  para que la clave no sea siempre la misma, ya que tomas las primeras imágenes
		shuffle($codigo);
		
		// con las primeras imágenes crea el código
		$principal = imagenPrincipal($tam, $codigo);
		
		// mezcla otra vez, para mostrarlas desordenadas
		shuffle($codigo);
			

		// genera todos los dibujos base64

		for ($x= 0; $x<=$cantidad-1;$x = $x+1 ){
		ob_start();
			ImagePng ($codigo[$x]["imagen"]);
					
			$codigo[$x]["base64"] = base64_encode(ob_get_clean());
			
			
			imagedestroy($codigo[$x]["imagen"]);
		}
		// genera la imagen principal base64
		ob_start();
			ImagePng ($principal["imagen"]);
			
			$principal["base64"] = base64_encode( ob_get_clean());
			
			imagedestroy($principal["imagen"]);

	
		
		
		
		
		
		// comienza con la visualización del formulario HTML
		

		
		$_SESSION['clavesesion'] = $principal["id"];
		$dibujo = $principal["base64"];
			
			
			
		//echo "<style>$estilo</style>";
		echo "<style rel='stylesheet' media='screen'>
				@import 'antirobots.css';
			</style>";
		
		echo "<div class = 'antirobot'>";
			echo "<p class='mensaje'> Selecciona las im&aacute;genes que aparecen </p>";
			echo" <img src='data:image/png;base64,$dibujo' class='principal' />";
			
			
					
				
				
				
				for ($x = 0; $x<= $cantidad-1; $x = $x+1) {	
					$dibujo = $codigo[$x]["base64"];
					
						echo "<input type=checkbox name='f$x' id='f$x'  />";
							echo "<label for='f$x'>";
							echo " <img src='data:image/png;base64,$dibujo' />";
							echo "</label>";
						
					//echo $codigo[$x]["nombre"];
					//echo "<br/>";
					echo "<input type = 'hidden' name ='c$x' value = '".$codigo[$x]['id']."' />";
				}
				
				
				
				
				echo "
				<input type = 'hidden' name= 'controlantirobot' value = 'comprobar' />
				<input type = 'hidden' name= 'cantidad' value = '$cantidad' />
				<input type = 'hidden' name= 'tam' value = '$tam' />
				
					";

		
		echo "</div>";
		
	}
	//echo "<label> hola <input type=checkbox value= hola / >asi es </label>";
	
	//$clave = pow(2, 10 );
	//echo str_shuffle("hola");
	//echo $principal["id"];
?>

	
	
	